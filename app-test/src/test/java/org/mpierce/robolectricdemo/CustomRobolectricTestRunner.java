package org.mpierce.robolectricdemo;

import org.junit.runners.model.InitializationError;
import org.robolectric.AndroidManifest;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.res.Fs;

import java.io.File;

public final class CustomRobolectricTestRunner extends RobolectricTestRunner {

    private static final int MAX_SDK_SUPPORTED_BY_ROBOLECTRIC = 18;

    public CustomRobolectricTestRunner(Class<?> testClass) throws InitializationError {
        super(testClass);
    }

    @Override
    protected AndroidManifest getAppManifest(Config config) {

        // this is used by gradle
        File baseDirRelativeToModule = new File("../app/src/main");
        File baseDirRelativeToRoot = new File("app/src/main");

        File baseDirToUse;

        if (baseDirRelativeToModule.exists()) {
            baseDirToUse = baseDirRelativeToModule;
        } else if (baseDirRelativeToRoot.exists()) {
            baseDirToUse = baseDirRelativeToRoot;
        } else {
            throw new RuntimeException(
                "Couldn't find base dir for android bits at " + baseDirRelativeToModule + " or " +
                    baseDirRelativeToRoot);
        }

        File manifest = new File(baseDirToUse, "AndroidManifest.xml");
        File res = new File(baseDirToUse, "res");

        return new AndroidManifest(Fs.newFile(manifest), Fs.newFile(res)) {
            @Override
            public int getTargetSdkVersion() {
                return MAX_SDK_SUPPORTED_BY_ROBOLECTRIC;
            }
        };
    }
}
