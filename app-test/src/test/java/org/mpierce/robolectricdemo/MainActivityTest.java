package org.mpierce.robolectricdemo;

import android.widget.Switch;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.util.ActivityController;

import static org.junit.Assert.assertEquals;
import static org.robolectric.Robolectric.buildActivity;

@RunWith(CustomRobolectricTestRunner.class)
public class MainActivityTest {

    private MainActivity activity;

    @Before
    public void setUp() throws Exception {
        activity = buildActivity(MainActivity.class).create().visible().get();

        ActivityController<MainActivity> controller = Robolectric.buildActivity(MainActivity.class);
        activity = controller.get();
        controller.create().start();
    }

    @Test
    public void testSwitchDoesSomething() throws Exception {
        Switch view = (Switch) activity.findViewById(R.id.switch_me);
        view.toggle();
        assertEquals(1, activity.getSwitchToggles());
    }

}
