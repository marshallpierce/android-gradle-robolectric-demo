package org.mpierce.robolectricdemo;

import android.test.ActivityInstrumentationTestCase2;
import com.google.android.apps.common.testing.ui.espresso.Espresso;
import com.google.android.apps.common.testing.ui.espresso.action.ViewActions;
import com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers;

public final class MainActivityAndroidTest extends ActivityInstrumentationTestCase2<MainActivity> {
    public MainActivityAndroidTest() {
        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    public void testFlipSwitch() {
        MainActivity activity = getActivity();

        Espresso.onView(ViewMatchers.withId(R.id.switch_me))
            .perform(ViewActions.click());

        assertEquals(1, activity.getSwitchToggles());
    }
}
