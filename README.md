Issues:

- Gradle import into IntelliJ puts junit-4.11 lower than the Android SDK, which causes issues when running Robolectric tests: `!!! JUnit version 3.8 or later expected`
